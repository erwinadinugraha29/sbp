<?php
$jenis_kendaraan = $_POST['jenis_kendaraan'];
$lampu = $_POST['lampu'];
$sim = $_POST['sim'];
$pelanggaran = $_POST['pelanggaran'];
$persyaratan = $_POST['persyaratan'];
$mengemudi = $_POST['mengemudi'];
$aselole = "";
array_push($pelanggaran,$lampu,$sim,$persyaratan,$mengemudi,$jenis_kendaraan);

$R1 = "<div><h3>Denda Maksimal Rp. 100.000 \n</h3></div>";
$R2 = "<div><h3>Denda Maksimal Rp. 250.000 \n</h3></div>";
$R3 = "<div><h3>Denda Maksimal Rp. 500.000 \n</h3></div>";
$R4 = "<div><h3>Denda Maksimal Rp. 750.000 \n</h3></div>";
$R5 = "<div><h3>Denda Maksimal Rp. 1.000.000 \n</h3></div>";
$R6 = "<div><h3>Anda Tidak Kena Denda \n</h3></div>"

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">
        <link rel="stylesheet" href="wizard.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
     <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">
        <link rel="stylesheet" href="wizard.css">
        <style type="text/css">
            .fa{
                margin-left: 11px;
            }
        </style>
    </head>
    
       
    <body style="text-align: center;">
  
        <div class="container" style="margin-top: 10%">
        <div class="row">
             <div class="card text-center">
              <div class="card-body">
                <div class="col-md-12 col-md-offset-0">
                    <h2><p>Hasil Penentuan Besar Sanksi Pelanggaran Lalu Lintas</p></h2>
                    
                    <img src="icon.png" alt="Italian Trulli" id="pulici">
                    <?php 
                    
                    //R1
                    if(in_array("a1", $pelanggaran) && in_array("a15", $pelanggaran) ){
                        echo $R1;
                        $aselole = "1";
                    }

                    //R2
                    if(((in_array("a1", $pelanggaran) || in_array("a2", $pelanggaran)) && (in_array("a4", $pelanggaran) ||in_array("a13", $pelanggaran))) || 
                    (in_array("a1", $pelanggaran) && (in_array("a6", $pelanggaran) || in_array("a12", $pelanggaran) || in_array("a15", $pelanggaran)) ) ||
                    (in_array("a2", $pelanggaran) && (in_array("a7", $pelanggaran) || in_array("a11", $pelanggaran) ) )){
                        $aselole = "1";
                        echo $R2;
                    }

                    //R3
                    if((in_array("a1", $pelanggaran) || in_array("a2", $pelanggaran)) && (in_array("a5", $pelanggaran) || in_array("a8", $pelanggaran) || in_array("a9", $pelanggaran) || in_array("a10", $pelanggaran)) || (in_array("a2", $pelanggaran) && in_array("a6", $pelanggaran)) ){
                        echo $R3;
                        $aselole = "1";
                    }

                    //R4
                    if((in_array("a1", $pelanggaran) || in_array("a2", $pelanggaran)) && in_array("a16", $pelanggaran)  ){
                        echo $R4;
                        $aselole = "1";
                    }

                    //R5
                    if((in_array("a1", $pelanggaran) || in_array("a2", $pelanggaran)) && in_array("a3", $pelanggaran)  ){
                        echo $R5;
                        $aselole = "1";
                    }

                    if($aselole === ""){
                        echo $R6;
                    }
                    ?>
                </div>
              </div>
              <div class="card-footer text-muted">
                Kelompok 1 ISBD © 2021
              </div>
            </div>
                
            </div>
        </div>
        <!-- Javascript -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script src="wizard.js"></script>
       
    </body>

</html>