<div style="text-align:right">
  <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Tambah Data</button>
</div>
<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Kode</th>
      <th scope="col">Kondisi</th>
      <th scope="col">Hasil</th>
      <th scope="col" colspan="2">Aksi</th>
    </tr>
  </thead>
  <tbody>
  	<?php
  	$sql = "SELECT * FROM rule order by id asc";
	$result = mysqli_query($conn, $sql);
	$no = 1;
  	while($row = mysqli_fetch_assoc($result)) {
  	?>
  	<tr>
  		<td><?=$no?></td>
  		<td><?=$row['id']?></td>
  		<td><?=$row['kondisi']?></td>
  		<td><?=$row['kode_sanksi']?></td>
  		<td><i class="fa fa-pencil"></i></td><td><i class="fa fa-times"></i></td>
  	</tr>
  	<?php
  	$no++;
  	}
  	?>
  </tbody>
</table>