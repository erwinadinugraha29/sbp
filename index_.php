<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">
        <link rel="stylesheet" href="wizard.css">
    </head>
    
       
    <body style="text-align: center;">
  
        <div class="container" >
            <div class="row">
                <div class="col-md-12 col-md-offset-0">
                <form action="proses.php" method="post" class="f1 border">
                		<h2><p>Penentuan Besar Sanksi Pelanggaran Lalu Lintas dengan Metode Forward Chaining</p></h2>
                		<div class="f1-steps">
                			<div class="f1-progress">
                			    <div class="f1-progress-line" data-now-value="25" data-number-of-steps="4" style="width: 25%;"></div>
                			</div>
                            <div class="f1-step active">
                                <div class="f1-step-icon"><i class="fa fa-id-card"></i></div>
                                <p>Form 1</p>
                            </div>
                			<div class="f1-step">
                				<div class="f1-step-icon"><i class="fa fa-car"></i></div>
                				<p>Form 2</p>
                			</div>
                			<div class="f1-step">
                				<div class="f1-step-icon"><i class="fa fa-road"></i></div>
                				<p>Form 3</p>
                			</div>
                		    <div class="f1-step">
                				<div class="f1-step-icon"><i class="fa fa-address-book"></i></div>
                				<p>Form 4</p>
                			</div>
                		</div>
                		<!-- step 1 -->
                		<fieldset>
                		    
                			<div class="form-group">
                            <h4>Apa Jenis Kendaraan Anda?</h4>
                                <div><input class="form-check-input" type="radio" name="jenis_kendaraan" value="a1" id="motor" >
                                <label class="form-check-label">
                                    Sepeda Motor
                                </label></div>
                                <div><input class="form-check-input" type="radio" name="jenis_kendaraan" value="a2" id="nomotor" CHECKED>
                                <label class="form-check-label">
                                    Selain Sepeda Motor
                                </label></div>
                            </div>
                            <div class="form-group motor" id="silampu" hidden>
                			    <h4>Apakah Anda tidak menyalakan lampu utama pada siang hari</h4> 
                                <div><input class="form-check-input" type="radio" name="lampu" value="a14" CHECKED>
                                <label class="form-check-label">
                                    Ya
                                </label></div>
                                <div><input class="form-check-input" type="radio" name="lampu" value="">
                                <label class="form-check-label">
                                    Tidak
                                </label></div>
                            </div>
                            <div class="form-group">
                			    <h4>Kepemilikan SIM</h4> 
                                <div><input class="form-check-input" type="radio" name="sim" value="a3" CHECKED>
                                <label class="form-check-label">
                                Tidak memiliki SIM 
                                </label></div>
                                <div><input class="form-check-input" type="radio" name="sim" value="a4">
                                <label class="form-check-label">
                                Memiliki SIM namun tak dapat menunjukkannya saat razia
                                </label></div>
                                <div><input class="form-check-input" type="radio" name="sim" value="" >
                                <label class="form-check-label">
                                Tidak Keduanya
                                </label></div>
                            </div>
                            <div class="f1-buttons">
                                <button type="button" class="btn btn-primary btn-next">Selanjutnya <i class="fa fa-arrow-right"></i></button>
                            </div>
                        </fieldset>
                        <!-- step 2 -->
                        <fieldset>
                            <div class="form-group" required>
                            <h4>Apakah Anda melakukan pelanggaran dibawah ini :</h4> 
                            <div><input class="form-check-input" type="checkbox" value="a13" id="flexCheckChecked" name="pelanggaran[]">
                            <label class="form-check-label" for="flexCheckChecked">
                            Tidak menyalakan lampu utama pada malam hari
                            </label></div>
                            <div class="motor" hidden><input class="form-check-input" type="checkbox" value="a12" id="flexCheckChecked" name="pelanggaran[]">
                            <label class="form-check-label" for="flexCheckChecked">
                            Tidak mengenakan helm standar nasional 
                            </label></div>
                            <div class="motor" hidden><input class="form-check-input" type="checkbox" value="a15" id="flexCheckChecked" name="pelanggaran[]">
                            <label class="form-check-label" for="flexCheckChecked">
                            Berbelok atau balik arah tanpa memberi isyarat lampu
                            </label></div>
                            <div><input class="form-check-input" type="checkbox" value="a7" id="flexCheckChecked" name="pelanggaran[]">
                            <label class="form-check-label" for="flexCheckChecked">
                            Tidak dilengkapi dengan perlengkapan berupa ban cadangan, segitiga pengaman, dongkrak, pembuka roda, dan peralatan pertolongan pertama pada kecelakaan
                            </label></div>
                            <div><input class="form-check-input" type="checkbox" value="a11" id="flexCheckChecked" name="pelanggaran[]">
                            <label class="form-check-label" for="flexCheckChecked">
                            Pengemudi atau penumpang yang duduk disamping pengemudi tak mengenakan sabuk keselamatan
                            </label></div>
                            <div><input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" name="pelanggaran[]">
                            <label class="form-check-label" for="flexCheckChecked">
                            Tidak Semuanya
                            </label></div>
                            <div hidden><input CHECKED class="form-check-input" type="checkbox" value="xxxxx" id="flexCheckChecked" name="pelanggaran[]">
                            <label class="form-check-label" for="flexCheckChecked" >
                            INIT ARAY BIAR GA ERROR
                            </label></div>
                            </div>
                            <div class="f1-buttons">
                                <button type="button" class="btn btn-warning btn-previous"><i class="fa fa-arrow-left"></i> Sebelumnya</button>
                                <button type="button" class="btn btn-primary btn-next">Selanjutnya <i class="fa fa-arrow-right"></i></button>
                            </div>
                        </fieldset>
                        <!-- step 3 -->
                        <fieldset>
                            <div class="form-group" required>
                            <h4>Apakah Anda melakukan pelanggaran dibawah ini :</h4>
                            <div><input class="form-check-input" type="checkbox" value="a5" id="flexCheckChecked2" name="pelanggaran[]">
                            <label class="form-check-label" for="flexCheckChecked2">
                            Tidak dipasangi Tanda Nomor Kendaraan
                            </label></div>
                            <div><input class="form-check-input" type="checkbox" value="a8" id="flexCheckChecked2" name="pelanggaran[]">
                            <label class="form-check-label" for="flexCheckChecked2">
                            Melanggar rambu lalu lintas
                            </label></div>
                            <div><input class="form-check-input" type="checkbox" value="a9" id="flexCheckChecked2" name="pelanggaran[]">
                            <label class="form-check-label" for="flexCheckChecked2">
                            Melanggar aturan batas kecepatan paling tinggi atau paling rendah
                            </label></div>
                            <div><input class="form-check-input" type="checkbox" value="a10" id="flexCheckChecked2" name="pelanggaran[]">
                            <label class="form-check-label" for="flexCheckChecked2">
                            Tidak dilengkapi STNK Bermotor atau STKC Bermotor
                            </label></div>
                            <div><input class="form-check-input" type="checkbox" value="" id="flexCheckChecked2" name="pelanggaran[]">
                            <label class="form-check-label" for="flexCheckChecked2">
                            Tidak Semuanya
                            </label></div>


                            </div>
                            <div class="f1-buttons">
                                <button type="button" class="btn btn-warning btn-previous"><i class="fa fa-arrow-left"></i> Sebelumnya</button>
                                <button type="button" class="btn btn-primary btn-next">Selanjutnya <i class="fa fa-arrow-right"></i></button>
                            </div>
                        </fieldset>
                        <!-- step 4 -->
                        <fieldset>
                            <div class="form-group">
                			    <h4>Apakah kendaraan Anda tidak memenuhi persyaratan teknis dan laik jalan? </h4> 
                                <div><input class="form-check-input" type="radio" name="persyaratan" value="a6" CHECKED>
                                <label class="form-check-label">
                                Ya 
                                </label></div>
                                <div><input class="form-check-input" type="radio" name="persyaratan" value="">
                                <label class="form-check-label">
                                Tidak
                                </label></div>
                                
                            </div>
                            <div class="form-group">
                			    <h4>Apakah Anda mengemudi dengan cara yang tidak wajar dan melakukan kegiatan yang menggangu konsentrasi dalam mengemudi </h4> 
                                <div><input class="form-check-input" type="radio" name="mengemudi" value="a16" CHECKED>
                                <label class="form-check-label">
                                Ya 
                                </label></div>
                                <div><input class="form-check-input" type="radio" name="mengemudi" value="">
                                <label class="form-check-label">
                                Tidak
                                </label></div>
                                
                            </div>

                            <div class="f1-buttons">
                                <button type="button" class="btn btn-warning btn-previous"><i class="fa fa-arrow-left"></i> Sebelumnya</button>
                                <button type="submit" class="btn btn-primary btn-submit"><i class="fa fa-save"></i> Submit</button>
                            </div>
                        </fieldset>
                	</form>
                </div>
            </div>
        </div>
        <!-- Javascript -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script src="wizard.js"></script>
       
    </body>

</html>