<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
     <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">
        <link rel="stylesheet" href="wizard.css">
        <style type="text/css">
            .fa{
                margin-left: 11px;
            }
        </style>
</head>
<body>
    <div class="container" style="margin-top: 10%">
        <div class="row">
          <?php
            if(!empty($_GET['m'])){
          ?>
            <div class="col-md-12">
                <div class="card">
                  <h5 class="card-header">Penentuan Besar Sanksi Pelanggaran Lalu Lintas</h5>
                  <div class="card-body">
                    <p class="card-text">
                        <form action="proses.php" method="post" class="f1">
                        <div class="f1-steps">
                            <div class="f1-progress">
                                <div class="f1-progress-line" data-now-value="25" data-number-of-steps="4" style="width: 25%;"></div>
                            </div>
                            <div class="f1-step active">
                                <div class="f1-step-icon"><i class="fa fa-id-card"></i></div>
                                <p>Step 1</p>
                            </div>
                            <div class="f1-step">
                                <div class="f1-step-icon"><i class="fa fa-car"></i></div>
                                <p>Step 2</p>
                            </div>
                            <div class="f1-step">
                                <div class="f1-step-icon"><i class="fa fa-road"></i></div>
                                <p>Step 3</p>
                            </div>
                            <div class="f1-step">
                                <div class="f1-step-icon"><i class="fa fa-address-book"></i></div>
                                <p>Step 4</p>
                            </div>
                        </div>
                        <!-- step 1 -->
                        <fieldset>                            
                            <div class="form-group">
                                <h4>Apa Jenis Kendaraan Anda?</h4>
                                <div class="list-group mx-0">
                                    <label class="list-group-item d-flex gap-2">
                                          <input class="form-check-input flex-shrink-0" type="radio" name="jenis_kendaraan" value="a1" id="motor">
                                          <span>
                                           Sepeda Motor
                                          </span>
                                    </label>
                                    <label class="list-group-item d-flex gap-2">
                                          <input class="form-check-input flex-shrink-0" type="radio" name="jenis_kendaraan" value="a2" id="nomotor">
                                          <span>
                                           Selain Sepeda Motor
                                          </span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group motor" id="silampu" hidden>
                                <h4>Apakah Anda tidak menyalakan lampu utama pada siang hari</h4> 
                                <div class="list-group mx-0">
                                    <label class="list-group-item d-flex gap-2">
                                          <input class="form-check-input flex-shrink-0" type="radio"  name="lampu" value="a14" CHECKED>
                                          <span>
                                           Ya
                                          </span>
                                    </label>
                                    <label class="list-group-item d-flex gap-2">
                                          <input class="form-check-input flex-shrink-0" type="radio" name="lampu" value="">
                                          <span>
                                           Tidak
                                          </span>
                                    </label>
                                </div>                                
                            </div>
                            <div class="form-group">
                                <h4>Kepemilikan SIM</h4> 
                                <div class="list-group mx-0">
                                    <label class="list-group-item d-flex gap-2">
                                          <input class="form-check-input flex-shrink-0" type="radio"  name="sim" value="a3" checked>
                                          <span>
                                           Tidak memiliki SIM 
                                          </span>
                                    </label>
                                    <label class="list-group-item d-flex gap-2">
                                          <input class="form-check-input flex-shrink-0" type="radio" name="sim" value="a4">
                                          <span>
                                            Memiliki SIM namun tak dapat menunjukkannya saat razia
                                          </span>
                                    </label>
                                    <label class="list-group-item d-flex gap-2">
                                          <input class="form-check-input flex-shrink-0" type="radio" name="sim" value="">
                                          <span>
                                            Tidak Keduanya
                                          </span>
                                    </label>
                                </div>   
                            </div>
                            <div class="f1-buttons" style="margin-top:20px">
                              <a href="index.php" class="btn btn-primary btn-next"><i class="fa fa-home"></i>   Home</a>
                                <button type="button" class="btn btn-primary btn-next">Selanjutnya <i class="fa fa-arrow-right"></i></button>
                            </div>
                        </fieldset>
                        <!-- step 2 -->
                        <fieldset>
                            <div class="form-group" required>
                            <h4>Apakah Anda melakukan pelanggaran dibawah ini : (1)</h4> 
                            <label class="list-group-item d-flex gap-2">
                              <input class="form-check-input flex-shrink-0" type="checkbox" value="a13" id="flexCheckChecked" name="pelanggaran[]">
                              <span>
                                Tidak menyalakan lampu utama pada malam hari
                              </span>
                            </label>
                            <label class="list-group-item d-flex gap-2">
                              <input class="form-check-input flex-shrink-0" type="checkbox" value="a12" id="flexCheckChecked" name="pelanggaran[]">
                              <span>
                                Tidak mengenakan helm standar nasional
                              </span>
                            </label>
                            <label class="list-group-item d-flex gap-2">
                              <input class="form-check-input flex-shrink-0" type="checkbox" value="a12" id="flexCheckChecked" name="pelanggaran[]">
                              <span>
                                Berbelok atau balik arah tanpa memberi isyarat lampu
                              </span>
                            </label>
                            <label class="list-group-item d-flex gap-2">
                              <input class="form-check-input flex-shrink-0" type="checkbox" value="a7" id="flexCheckChecked" name="pelanggaran[]">
                              <span>
                                Tidak dilengkapi dengan perlengkapan berupa ban cadangan, segitiga pengaman, dongkrak, pembuka roda, dan peralatan pertolongan pertama pada kecelakaan
                              </span>
                            </label>
                            <label class="list-group-item d-flex gap-2">
                              <input class="form-check-input flex-shrink-0" type="checkbox" value="a11" id="flexCheckChecked" name="pelanggaran[]">
                              <span>
                                Pengemudi atau penumpang yang duduk disamping pengemudi tak mengenakan sabuk keselamatan
                              </span>
                            </label>
                            <label class="list-group-item d-flex gap-2">
                              <input class="form-check-input flex-shrink-0" type="checkbox" value="" id="flexCheckChecked" name="pelanggaran[]">
                              <span>
                                Tidak Semuanya
                              </span>
                            </label>

                            <div hidden><input CHECKED class="form-check-input" type="checkbox" value="xxxxx" id="flexCheckChecked" name="pelanggaran[]">
                            <label class="form-check-label" for="flexCheckChecked" >
                            INIT ARAY BIAR GA ERROR
                            </label></div>
                            </div>
                            <div class="f1-buttons" style="margin-top:20px">
                                <button type="button" class="btn btn-warning btn-previous"><i class="fa fa-arrow-left"></i> Sebelumnya</button>
                                <button type="button" class="btn btn-primary btn-next">Selanjutnya <i class="fa fa-arrow-right"></i></button>
                            </div>
                        </fieldset>
                        <!-- step 3 -->
                        <fieldset>
                            <div class="form-group" required>
                            <h4>Apakah Anda melakukan pelanggaran dibawah ini : (2)</h4>
                            <label class="list-group-item d-flex gap-2">
                              <input class="form-check-input flex-shrink-0" type="checkbox" value="a5" id="flexCheckChecked2" name="pelanggaran[]">
                              <span>
                                Tidak dipasangi Tanda Nomor Kendaraan
                              </span>
                            </label>
                            <label class="list-group-item d-flex gap-2">
                              <input class="form-check-input flex-shrink-0" type="checkbox" value="a8" id="flexCheckChecked2" name="pelanggaran[]">
                              <span>
                                Melanggar rambu lalu lintas
                              </span>
                            </label>
                            <label class="list-group-item d-flex gap-2">
                              <input class="form-check-input flex-shrink-0" type="checkbox" value="a9" id="flexCheckChecked2" name="pelanggaran[]">
                              <span>
                                Melanggar aturan batas kecepatan paling tinggi atau paling rendah
                              </span>
                            </label>
                            <label class="list-group-item d-flex gap-2">
                              <input class="form-check-input flex-shrink-0" type="checkbox" value="a10" id="flexCheckChecked2" name="pelanggaran[]">
                              <span>
                                Tidak dilengkapi STNK Bermotor atau STKC Bermotor
                              </span>
                            </label>
                             <label class="list-group-item d-flex gap-2">
                              <input class="form-check-input flex-shrink-0" type="checkbox" value="" id="flexCheckChecked2" name="pelanggaran[]">
                              <span>
                                Tidak Semuanya
                              </span>
                            </label>
                            </div>
                            <div class="f1-buttons" style="margin-top:20px">
                                <button type="button" class="btn btn-warning btn-previous"><i class="fa fa-arrow-left"></i> Sebelumnya</button>
                                <button type="button" class="btn btn-primary btn-next">Selanjutnya <i class="fa fa-arrow-right"></i></button>
                            </div>
                        </fieldset>
                        <!-- step 4 -->
                        <fieldset>
                            <div class="form-group">
                                <h4>Apakah kendaraan Anda tidak memenuhi persyaratan teknis dan laik jalan? </h4>
                                <div class="list-group mx-0">
                                    <label class="list-group-item d-flex gap-2">
                                          <input class="form-check-input flex-shrink-0" type="radio" name="persyaratan" value="a6" CHECKED>
                                          <span>
                                           Ya
                                          </span>
                                    </label>
                                    <label class="list-group-item d-flex gap-2">
                                          <input class="form-check-input flex-shrink-0" type="radio" name="persyaratan" value="">
                                          <span>
                                           Tidak
                                          </span>
                                    </label>
                                </div>                                
                            </div>
                            <div class="form-group">
                                <h4>Apakah Anda mengemudi dengan cara yang tidak wajar dan melakukan kegiatan yang menggangu konsentrasi dalam mengemudi </h4>
                                <div class="list-group mx-0">
                                    <label class="list-group-item d-flex gap-2">
                                          <input class="form-check-input flex-shrink-0" type="radio"  name="mengemudi" value="a16" CHECKED>
                                          <span>
                                           Ya
                                          </span>
                                    </label>
                                    <label class="list-group-item d-flex gap-2">
                                          <input class="form-check-input flex-shrink-0" type="radio" name="mengemudi" value="">
                                          <span>
                                           Tidak
                                          </span>
                                    </label>
                                </div>                                 
                            </div>

                            <div class="f1-buttons" style="margin-top:20px">
                                <button type="button" class="btn btn-warning btn-previous"><i class="fa fa-arrow-left"></i> Sebelumnya</button>
                                <button type="submit" class="btn btn-primary btn-submit"><i class="fa fa-save"></i> Submit</button>
                            </div>
                        </fieldset>
                        </form>
                    </p>
                  </div>
                  <div class="card-footer">
                    <small class="text-muted">Kelompok 1 ISBD © 2021</small>
                  </div>

                </div>
            </div>
            <?php
            }else{
            ?>
            <div class="card text-center">
              <div class="card-header">
                <ul class="nav nav-tabs card-header-tabs">
                  <li class="nav-item">
                    <a class="nav-link <?=empty($_GET['menu'])||$_GET['menu']==''?'active':''?>" href="index.php">Home</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link <?=!empty($_GET['menu'])&&$_GET['menu']=='pelanggaran'?'active':''?>" href="index.php?menu=pelanggaran">Pelanggaran</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link <?=!empty($_GET['menu'])&&$_GET['menu']=='sanksi'?'active':''?>" href="index.php?menu=sanksi">Sanksi</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link <?=!empty($_GET['menu'])&&$_GET['menu']=='rule'?'active':''?>" href="index.php?menu=rule">Rule</a>
                  </li>
                </ul>
              </div>
              <div class="card-body">
                <?php
                include"koneksi.php";
                if(empty($_GET['menu'])){
                  $_GET['menu'] = "";
                }
                switch($_GET['menu']){
                  case"pelanggaran":
                  include"pelanggaran.php";
                  break;
                  case"sanksi":
                  include"sanksi.php";
                  break;
                  case"rule":
                  include"rule.php";
                  break;
                  default:
                  ?>
                  <h5 class="card-title">Penentuan Besar Sanksi Pelanggaran Lalu Lintas</h5>
                  <p class="card-text">
                    Dengan Metode Forward Chaining<br>
                    <img src="icon.png">
                  </p>
                  <a href="index.php?m=1" class="btn btn-primary btn-lg">Mulai</a>
                  <?php
                  break;
                }
                ?>
              </div>
              <div class="card-footer text-muted">
                Kelompok 1 ISBD © 2021
              </div>
            </div>
            <?php
            }
            ?>
        </div>
    </div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="wizard.js"></script>
</body>
</html>