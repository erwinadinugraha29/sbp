-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 31, 2021 at 04:21 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sbp`
--

-- --------------------------------------------------------

--
-- Table structure for table `pelanggaran`
--

CREATE TABLE `pelanggaran` (
  `kode_pelanggaran` char(4) NOT NULL,
  `pelanggaran` text NOT NULL,
  `pertanyaan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pelanggaran`
--

INSERT INTO `pelanggaran` (`kode_pelanggaran`, `pelanggaran`, `pertanyaan`) VALUES
('A1', 'Sepeda Motor', 'Apa Jenis Kendaraan Anda?'),
('A10', 'Tidak dilengkapi STNK Bermotor atau STKC Bermotor', 'Apakah Anda melakukan pelanggaran dibawah ini : (2)'),
('A11', 'Pengemudi atau penumpang yang duduk disamping pengemudi tak mengenakan sabuk keselamatan', 'Apakah Anda melakukan pelanggaran dibawah ini : (1)'),
('A12', 'Tidak mengenakan helm standar nasional', 'Apakah Anda melakukan pelanggaran dibawah ini : (1)'),
('A13', 'Tidak menyalakan lampu utama pada malam hari', 'Apakah Anda melakukan pelanggaran dibawah ini : (1)'),
('A14', 'Tidak menyalakan lampu utama pada siang hari', 'Apakah Anda tidak menyalakan lampu utama pada siang hari'),
('A15', 'Berbelok atau balik arah tanpa memberi isyarat lampu', 'Apakah Anda melakukan pelanggaran dibawah ini : (1)'),
('A16', 'Mengemudi dengancara yang tidak wajardan melakukan kegiatan yang menggangu konsentrasi    dalam mengemudi', 'Apakah Anda mengemudi dengan cara yang tidak wajar dan melakukan kegiatan yang menggangu konsentrasi dalam mengemudi'),
('A2', 'Selain Sepeda Motor', 'Apa Jenis Kendaraan Anda?'),
('A3', 'Tidak memiliki SIM', 'Kepemilikan SIM'),
('A4', 'Memiliki SIM namun tak dapat menunjukkannya saat razia', 'Kepemilikan SIM'),
('A5', 'Tidak dipasangi Tanda Nomor Kendaraan', 'Apakah Anda melakukan pelanggaran dibawah ini : (2)'),
('A6', 'Tidak memenuhi persyaratan teknis dan laik jalan', 'Apakah kendaraan Anda tidak memenuhi persyaratan teknis dan laik jalan? '),
('A7', 'Tidak dilengkapi dengan perlengkapan berupa ban cadangan, segitiga pengaman, dongkrak, pembuka roda, dan peralatan pertolongan pertama pada kecelakaa', 'Apakah Anda melakukan pelanggaran dibawah ini : (1)'),
('A8', 'Melanggar rambu lalu lintas', 'Apakah Anda melakukan pelanggaran dibawah ini : (2)'),
('A9', 'Melanggar aturan batas kecepatan paling tinggi atau paling rendah', 'Apakah Anda melakukan pelanggaran dibawah ini : (2)');

-- --------------------------------------------------------

--
-- Table structure for table `rule`
--

CREATE TABLE `rule` (
  `id` char(4) NOT NULL,
  `kondisi` text NOT NULL,
  `kode_sanksi` char(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `rule`
--

INSERT INTO `rule` (`id`, `kondisi`, `kode_sanksi`) VALUES
('R0', 'A1 OR A2', 'R0'),
('R1', 'A1 AND A14', 'P1'),
('R2', '(R0 AND (A4 OR A13)) OR (A1 AND (A6 OR A12 OR A15)) \r\nOR (A2 AND (A7 OR A11))', 'P2'),
('R3', '(R0 AND (A4 OR A13) ) OR (A1 AND (A6 OR A12 OR A15)) \r\n         OR (A2 AND (A7 OR A11))', 'P3'),
('R4', 'R0 AND A16', 'P4'),
('R5', 'R0 AND A3', 'P5');

-- --------------------------------------------------------

--
-- Table structure for table `sanksi`
--

CREATE TABLE `sanksi` (
  `kode_sanksi` char(4) NOT NULL,
  `besar_sanksi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sanksi`
--

INSERT INTO `sanksi` (`kode_sanksi`, `besar_sanksi`) VALUES
('P1', 100000),
('P2', 250000),
('P3', 500000),
('P4', 750000),
('P5', 1000000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pelanggaran`
--
ALTER TABLE `pelanggaran`
  ADD PRIMARY KEY (`kode_pelanggaran`);

--
-- Indexes for table `rule`
--
ALTER TABLE `rule`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
